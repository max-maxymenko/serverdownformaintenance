<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Server is down for Maintenance</title>
<style>
	html,body {background:#eee;}
	div#content_wrapper {background:rgba(255,255,255,0.8); width:1000px; max-width:90%; margin:auto; margin-top:15%; padding:30px;}
	div#content_wrapper div#title {text-transform:uppercase; text-align:center; font-size:25px; color:#555; margin-bottom:20px;}
	div#content_wrapper div#content {}
	div#content_wrapper div#content div#message {text-align:center;}
	div#content_wrapper div#content div#notify_wrapper {padding:30px 50px 0px 30px;}
	div#content_wrapper div#content div#notify_wrapper div#email_wrapper {display:flex; align-items:center;}
	div#content_wrapper div#content div#notify_wrapper div#email_wrapper input#email {width:100%;}
	div#content_wrapper div#content div#notify_wrapper div#email_wrapper button#btnSubmit {width:150px; margin-left:5px;}
</style>
<link rel="stylesheet" href="./src/main.min.css"/>
</head>
<body>
	<div id="content_wrapper">
		<div id="title">Server is down for maintenance</div>
		<div id="content">
			<div id="message">
				It appears that the server of the website you are trying to visit is currently undergoing a scheduled maintenance. <br/> Please try again later or leave your email below and we will notify you once server is back online!
			</div>
			<div id="notify_wrapper">
				<div id="status"><p style="color:#bbb; font-size:10px; font-weight:bold; text-transform:uppercase;">To get updated once the server is back online, please leave your email below:</p></div>
				<div id="email_wrapper">
					<input id="email" class="input input-default default input-big" placeholder="Email Address"/>
					<button id="btnSubmit" class="btn btn-material btn-material-success">Let me know</button>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script async="async" src="./src/main.js"></script>
</html>