<?php
/**
 *	Current class used as a container to store 'resource' content of the file.
 *
 * 	@author Max Maxymenko
 *	@date 13/02/2017
 *	@link https://bitbucket.org/snippets/max-maxymenko/A5rpe
 */
class FileIOResource
{
	public $resource; // DT: 'resource'
}

/**
 *	Current class is used for handling the File IO.
 *	It relies on "FileIOResource" Class for temporary storing file resource in.
 *
 *	Class takes care of:
 *	1. Files
 *	2. Folders/Directories
 *
 *	@author Max Maxymenko
 *	@date 4/11/2016
 *	@modified 04/07/2017
 *	@link https://bitbucket.org/snippets/max-maxymenko/A5rpe
 */
class FileIO
{
	private $name = NULL; // DT: "string"
	
	public function __construct()
	{
		// do nothing
	}
	
	/**
	 *	Setter for name
	 *	NOTE: The only reason this function is public is to allow multiple files to use current obj.
	 *
	 *	@param String $name
	 *	@return none
	 */
	private function setName( String $name ) // THROWS Exception
	{
		if (trim($name) == '')
			throw new Exception('FileIO - Invalid File/Folder Name');
			
			$this->name = htmlspecialchars( trim( $name ) );
	}
	
	
	// Handle File
	/**
	 *	Function used to open the file.
	 *	Flags by default are set to 'c+' = reading and writing
	 *
	 *	@param String $name
	 *	@param String $flags
	 *	@return none
	 */
	public function openFile( String $fileName, String $flags='c+' ) : FileIOResource // THOROWS: E_WARNING & Exception
	{
		$fileResource = NULL;
		$this->setName($fileName);
		
		// if the file does not exists, we need to create it
		if (!$this->checkFileExists($fileName)) {
			//throw new Exception('File does not exist in the specified folder.');
			$this->touchFile( $this->name ); // create a file
		}
		
		if ( !$this->checkFilePermissions('r') )
			throw new Exception('File "' . $fileName. '" is not readable. Check that you have sufficient permissions.');
		
		try
		{
			$fileResource = new FileIOResource();
			$fileResource->resource = fopen( $this->name, $flags );
		}
		catch ( E_WARNING $e )
		{
			throw new Exception( 'Unable to open file "'.$name.'". Error: ' . $e->getMessage() );
		}
		catch ( Exception $e )
		{
			throw new Exception( 'Could not open file. Error: ' . $e->getMessage() );
		}
		
		return $fileResource;
	}
	
	
	/**
	 *	Function responsible for reading the file contents
	 *
	 *	@param none
	 *	@return String
	 */
	public function readFile() : String
	{
		if (trim($this->name) == '')
			throw new Exception('FileIO - File/Folder Name is not set');
			
		if ( !$this->checkFilePermissions('r') )
			throw new Exception('File "' . $this->name . '" is not readable. Check that you have sufficient permissions.');
		
		return @file_get_contents($this->name);
	}
	
	/**
	 *	A function which is responsible for writing to the file.
	 *
	 *	@param String $name
	 *	@param String $contents
	 *	@return bool
	 */
	public function writeToFile( FileIOResource $fileResource, String $contents ) : bool // THROWS: Exception
	{
		if ( !$this->checkFilePermissions('w') )
			throw new Exception('File "' . $this->name . '" is not writable. Check that you have sufficient permissions.');
			
		$this->clearFileContents();
		return (fwrite( $fileResource->resource, $contents )) ? true : false;
	}
	
	/**
	 *	A function is responsible for adding an extra contents to an existing file.
	 *	It opens up a function and adds contents which is provided by the paramater to the file.
	 *
	 *	@param String $name
	 *	@param String $contents
	 *	@param String $position
	 *	@return bool
	 */
	public function appendToFile( FileIOResource $fileResource, String $contents, String $position='end' ) : bool // THROWS Exception
	{
		if ( $contents == NULL || trim($contents) == '')
			throw new Exception('FileIO - No content is given to be appended to the file');
		
		if ( !$this->checkFilePermissions('w') )
			throw new Exception('File "' . $this->name . '" is not writable. Check that you have sufficient permissions.');
				
		$file_contents = $this->readFile();
		
		//check the position of the text to be added (start/end)
		if ( ($position == 'start') || ($position == 'beginning'))
			$newContent = $contents."\n".$file_contents;
		else
			$newContent = $file_contents."\n".$contents;
			
		return (fwrite( $fileResource->resource, $newContent )) ? true : false;
	}
	
	/**
	 *	Function used to 'touch' the file, changes its timestamp.
	 *
	 *	@param String $fileName
	 *	@return bool
	 */
	public function touchFile( String $fileName ) : bool
	{
		return touch( $fileName );
	}
	
	/**
	 *	Function used to close an open file.
	 *
	 *	@param none
	 *	@return boolean
	 */
	public function closeFile( FileIOResource $fileResource ) : bool
	{
		return ( fclose( $fileResource->resource ) ) ? true : false;
	}
	
	/**
	 *	A function used to erase the contents of the file.
	 *
	 *	@param none
	 *	@return none
	 */
	private function clearFileContents()
	{
		if ( strlen($this->readFile()) > 0)
			@file_put_contents($this->name, '');
	}
	
	/**
	 *	Function used to check a file/dir for sufficient permmissions.
	 *
	 *	@param String $permStr
	 *	@return bool
	 */
	private function checkFilePermissions( String $permStr ) : bool
	{
		switch ( $permStr ){
			case 'r' : return is_readable( $this->name );
			case 'w' : return is_writable( $this->name );
		}
		return false;
	}
	
	
	// File Helper Functions
	
	/**
	 *	The function bellow is responsible for getting a file extension.
	 *
	 *	@param String $name
	 *	@return string
	 */
	public static function getFileExtension( String $fileName )
	{
		return end(explode(".", strtolower($fileName)));
	}
	
	/**
	 * 	Public function checks if the file exists and is readable.
	 *
	 * 	@param String $fileName
	 * 	@return boolean
	 */
	public function checkFileExists( String $fileName )
	{
		return (file_exists($fileName)) ? true : false;
	}
	
	// Handle Directories
	
	/**
	 * 	Public function checks if the directory exists.
	 *
	 * 	@param String $dirName
	 * 	@return boolean
	 */
	public function checkDirectoryExists( String $dirName ) : bool
	{
		return (is_dir($dirName)) ? true : false;
	}
	
	/**
	 * 	Function responsible for creating a directory.
	 *
	 * 	@param String $dirName
	 * 	@param String $permMode
	 *  @throws Exception
	 * 	@return bool
	 */
	public function createDirectory( String $dirName, int $permMode = 0777 ) : bool
	{
		$dirName = htmlspecialchars($dirName);
		if ($this->checkDirectoryExists( $dirName ))
			throw new Exception('Directory "' . $dirName . '" already exists.');
			return mkdir($dirName, $permMode);
	}
	
	/**
	 *	Function responsible for deleting a directory.
	 *
	 *	@param String $dirName
	 * 	@throws Exception
	 * 	@return bool
	 */
	public function deleteDirectory( String $dirName ) : bool
	{
		$dirName = htmlspecialchars($dirName);
		if (!$this->checkDirectoryExists( $dirName ))
			throw new Exception('Directory "' . $dirName . '" does not exists.');
			return rmdir($dirName);
	}
	
}// end class
?>