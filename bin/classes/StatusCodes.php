<?php
class StatusCodes
{
	const SUCCESS = 200;
	const BAD_REQUEST = 400;
	const UNAUTHORISED = 401;
	const FORBIDDEN = 403;
	const ERROR = 500;
}
?>