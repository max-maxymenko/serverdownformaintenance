/**
 *	Main JS
 * 
 * 	@author Max Maxymenko
 * 	@date 02/07/2017
 * 	@modified 03/07/2017
 */

/**
 *	Function used to handle the #btnSubmit on-click event.
 *	Takes the email from the input field and posts it to the server-side script.
 *
 *	@param none
 *	@return none
 */
$('button#btnSubmit').click(function(){
	var $wrapper = $('div#notify_wrapper');
	var $status = $wrapper.find('div#status');
	var $email = $wrapper.find('input#email');
	
	if ( $email.val().trim() == '' || !validateEmail($email.val()) ) {
		$status.html('<div class="msg msg-danger">Please enter a valid email</div>');
	}
	else {
		$status.html('<img src="./src/loader.gif"/>');
		$.ajax({
			type:'post',
			url:'subscribe.php',
			dataType:'json',
			data:{
				email: $email.val().trim()
			},
			success:function(payload){
				if (payload.status == 200) // success
					$status.html('<div class="msg msg-success">' + payload.message + '</div>');
				else // error
					$status.html('<div class="msg msg-danger">' + payload.message + '</div>');
				
				$email.val(''); // reset the value of the email field
				$('div#email_wrapper').remove(); 
			},
			error:function(payload){
				$status.html('Error: An unexpected error has occurred');
				console.error(payload);
				alert('An error has occurred');
			}
		});
	}
	return false;
});

/**
 *	Function used to validate emails.
 * 
 *	@link https://bitbucket.org/snippets/max-maxymenko/B55xg/js-helpers
 *	@param String email
 * 	@returns boolean
 */
function validateEmail(email){
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test( email );
}