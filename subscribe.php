<?php
/**
 *	File used to handling the email which is posted to the script and recording it to the file.
 *
 *	@author Max Maxymenko
 *	@date 03/07/2017
 */
if (!include_once 'load.php')
	die('Cannot load core');

try {
	if (!isset($_POST['email']))
		throw new Exception('Missing Parameters');
	
	$dataStr = date('Y-m-d H:i:s') . "\t IP: " . $_SERVER['REMOTE_ADDR'] . "\t EMAIL: " . htmlspecialchars( trim($_POST['email']) );
	
	// write to a file
	try {
		$fileIO = new FileIO();
		$fileResource = $fileIO->openFile( $_LOG_FILE );
		if (!$fileIO->appendToFile($fileResource, $dataStr))
			throw new Exception('Could not record email');
		$fileIO->closeFile( $fileResource);
		
		$data['message'] = 'Thanks, we will notify you once the server is back online!';
	}
	finally {
		unset($fileIO);
	}
}
catch (Exception $e){
	$data['message'] = $e->getMessage();
	$data['status'] = StatusCodes::ERROR;
}
finally {
	echo json_encode($data);
}