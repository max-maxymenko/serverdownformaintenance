# README

A lightweight system mainly designed to let the users know about a scheduled server maintenance. The system also allows users to leave their email, which is then can be used to send out emails to let them know that the server is back online.

# Requrements

* WebServer (apache/nginx)
* PHP 7.0+
* Email Server (optional)

# Fiddle
UI Fiddle - https://jsfiddle.net/maxmaxymenko/46n9nunk/