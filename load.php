<?php
/**
 *	A core file responsible for loading core files.
 *
 *	@author Max Maxymenko
 *	@date 03/07/2017
 */
require_once __DIR__ . '/bin/classes/FileIO.php';
require_once __DIR__ . '/bin/classes/StatusCodes.php';

$_LOG_FILE = './emails.log';

$data = [
	'data' => [],
	'message' => '',
	'status' => StatusCodes::SUCCESS,
];